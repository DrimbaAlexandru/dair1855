package model;

public class Nota
{
	private int nrmatricol;
	private String materie;
	private int nota;

	public Nota( int nrmatricol, String materie, int nota )
	{
		this.setNrmatricol( nrmatricol );
		this.setMaterie( materie );
		this.setNota( nota );
	}

	public Nota(){}

    public int getNota()
    {
        return nota;
    }

    public int getNrmatricol()
    {
        return nrmatricol;
    }

    public String getMaterie()
    {
        return materie;
    }

    public void setMaterie( String materie )
    {
        this.materie = materie;
    }

    public void setNota( int nota )
    {
        this.nota = nota;
    }

    public void setNrmatricol( int nrmatricol )
    {
        this.nrmatricol = nrmatricol;
    }

    @Override
    public boolean equals( Object obj )
    {
        return ( obj instanceof Nota ) &&
                ( ( ( Nota )obj ).getMaterie().equals( getMaterie() ) ) &&
                ( ( ( Nota )obj ).getNota() == getNota() ) &&
                ( ( ( Nota )obj ).getNrmatricol() == getNrmatricol() );
    }
}
