package VVSS2018.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import model.Nota;
import org.openqa.selenium.WebElement;

@DefaultUrl("file://D:\\git\\VVSS\\dair1855\\HTML L5\\page.html")
public class MainPage extends PageObject {

    @FindBy( id = "nr_matricol_nota_add" )
    private WebElementFacade form_nr_matricol;

    @FindBy( id = "materie_nota_add" )
    private WebElementFacade form_materie;

    @FindBy( id = "nota_nota_add" )
    private WebElementFacade form_nota;

    @FindBy( id = "btn_add_nota" )
    private WebElementFacade btn_add_nota;

    @FindBy( id = "btn_update" )
    private WebElementFacade btn_update;

    public void enter_nota( Nota nota )
    {
        form_nr_matricol.type( "" + nota.getNrmatricol() );
        form_materie.type( nota.getMaterie() );
        form_nota.type( "" + nota.getNota() );
    }

    public void click_add_nota()
    {
        btn_add_nota.click();
    }

    public void click_update()
    {
        btn_update.click();
    }

    public Integer get_valid_nr_matricol()
    {
        WebElementFacade elevi_list = find( By.id( "tabel_elevi" ) );
        try
        {
            return Integer.parseInt( elevi_list.findElements( By.tagName( "tr" ) ).get( 1 ).findElements( By.tagName( "td" ) ).get( 0 ).getText() );
        }
        catch( Exception e )
        {
            return null;
        }
    }

    public boolean lookup_nota( Nota n )
    {
        WebElementFacade note_list = find( By.id( "tabel_nota" ) );
        List< Nota > note = new ArrayList<>();
        for( WebElement webElement : note_list.findElements( By.tagName( "tr" ) ) )
        {
            try
            {
                List< WebElement > tds = webElement.findElements( By.tagName( "td" ) );
                note.add( new Nota( Integer.parseInt( tds.get( 0 ).getText() ), tds.get( 1 ).getText(), Integer.parseInt( tds.get( 2 ).getText() ) ) );
            }
            catch( Exception e )
            {
            }
        }
        return note.contains( n );
    }

    public String getAlertMessageOrNothing()
    {
        try
        {
            return getAlert().getText();
        }
        catch( Exception e )
        {
            return "";
        }
    }

    public void dismissAlert()
    {
        getAlert().accept();
    }
}