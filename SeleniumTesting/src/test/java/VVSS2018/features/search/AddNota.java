package VVSS2018.features.search;

import VVSS2018.steps.serenity.EndUserSteps;
import model.Nota;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;
import org.jruby.util.Random;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

/**
 * Created by Alex on 16.05.2018.
 */

@RunWith( SerenityRunner.class )
public class AddNota
{
    @Managed( uniqueSession = true, driver = "chrome" )
    public WebDriver webdriver;

    @Steps
    public EndUserSteps anna;

    @Test
    public void add_valid_nota()
    {
        anna.is_the_home_page();
        Integer nr_matricol = anna.read_valid_nrMatricol();
        Nota addedNota = new Nota( nr_matricol, "Burakku Kurou", 10 );
        anna.entersAndSubmitsNota( addedNota );
        anna.should_see_alert_message( "Nota adaugata cu succes!" );
        anna.dismiss_alert();
        anna.hits_the_update_button();
        anna.should_see_nota( addedNota );
    }

    @Test
    public void add_invalid_nota()
    {
        anna.is_the_home_page();
        Integer nr_matricol = anna.read_valid_nrMatricol();
        Nota addedNota = new Nota( -1, "Hyaaaaa", 5 );
        anna.entersAndSubmitsNota( addedNota );
        anna.should_see_alert_message( "Numarul matricol introdus nu este corect" );
        anna.dismiss_alert();
        anna.hits_the_update_button();
        anna.should_not_see_nota( addedNota );
    }
}
