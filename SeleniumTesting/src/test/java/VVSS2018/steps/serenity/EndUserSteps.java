package VVSS2018.steps.serenity;

import VVSS2018.pages.MainPage;
import model.Nota;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.hamcrest.Matchers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;

public class EndUserSteps {

    MainPage mainPage;

    @Step
    public void entersAndSubmitsNota( Nota n )
    {
        mainPage.enter_nota( n );
        mainPage.click_add_nota();
    }

    @Step
    public int read_valid_nrMatricol()
    {
        Integer nrMatricol = mainPage.get_valid_nr_matricol();
        assertThat( nrMatricol, Matchers.notNullValue() );
        return nrMatricol;
    }

    @Step
    public void should_see_alert_message(String msg)
    {
        assertThat( mainPage.getAlertMessageOrNothing(), Matchers.equalTo( msg ) );
    }

    @Step
    public void should_see_nota( Nota n )
    {
        assertThat( mainPage.lookup_nota( n ), equalTo( true ) );
    }

    @Step
    public void should_not_see_nota( Nota n )
    {
        assertThat( mainPage.lookup_nota( n ), equalTo( false ) );
    }

    @Step
    public void is_the_home_page() {
        mainPage.open();
    }

    @Step
    public void dismiss_alert()
    {
        mainPage.dismissAlert();
    }

    @Step
    public void hits_the_update_button()
    {
        mainPage.click_update();
    }
}