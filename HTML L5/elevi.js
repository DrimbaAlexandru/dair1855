$(document).ready(table_updater());

function get_row_html_elev(elev)
{
    var row="";
    row+="<tr> \r";
    row+="<td>"+elev.nrmatricol+"</td> \r";
    row+="<td>"+elev.nume+"</td> \r";
    row+="</tr> \r";
    return row;
}

function get_row_html_nota(nota)
{
    var row="";
    row+="<tr> \r";
    row+="<td>"+nota.nrmatricol+"</td> \r";
    row+="<td>"+nota.materie+"</td> \r";
    row+="<td>"+nota.nota+"</td> \r";
    row+="</tr> \r";
    return row;
}

function set_rows_elevi( idTabel, elevi )
{
    var content="";
    content+=" <tr> \r <th>Nr. matricol</th> \r <th>Nume</th> \r </tr>";
    var i;
    for(i=0;i<elevi.length;i++)
    {
        content+=get_row_html_elev(elevi[i]);
    }
    content+="";
    $('#'+idTabel).html(content);
}

function set_rows_note( idTabel, note )
{
    var content="";
    content+=" <tr> \r <th>Nr. matricol</th> \r <th>Materie</th> \r <th>Nota</th> \r </tr>";
    var i;
    for(i=0;i<note.length;i++)
    {
        content+=get_row_html_nota(note[i]);
    }
    content+="";
    $('#'+idTabel).html(content);
}

function populate_table_elevi( idTabel )
{
    var elevi;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        elevi = JSON.parse(this.responseText);
        console.log(elevi);
        set_rows_elevi(idTabel,elevi);
        }
    };
    xhttp.open("GET", "http://localhost:8080/elev", true);
    xhttp.send();
}

function populate_table_note( idTabel )
{
    var note;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        note = JSON.parse(this.responseText);
        console.log(note);
        set_rows_note(idTabel,note);
        }
    };
    xhttp.open("GET", "http://localhost:8080/nota", true);
    xhttp.send();
}

function btn_add_nota()
{
    var nota = new Object();
    nota.nrmatricol = parseInt(document.getElementById("nr_matricol_nota_add").value);
    nota.materie = document.getElementById("materie_nota_add").value;
    nota.nota = parseInt(document.getElementById("nota_nota_add").value);
    
    console.log(JSON.stringify(nota));
    
    var xhttp = new XMLHttpRequest();
    
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        alert("Nota adaugata cu succes!");
        }
    if (this.readyState == 4 && this.status == 400) {
        var response = JSON.parse(this.responseText);
        alert(response.error);
        }
    };
    xhttp.open("POST", "http://localhost:8080/nota", true);
    xhttp.setRequestHeader('Content-Type', 'application/json')
    xhttp.send(JSON.stringify(nota));
}

function btn_update()
{
    populate_table_elevi('tabel_elevi');
    populate_table_note('tabel_nota');
}

function table_updater()
{
    btn_update();
    //setTimeout(function(){ table_updater() }, 5000);
}
