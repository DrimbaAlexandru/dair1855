package note.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javafx.util.Pair;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;

import note.repository.*;

import note.utils.ClasaException;
import note.utils.Constants;

public class NoteController
{
    private NoteRepository note;
    private EleviRepository elevi;

    public NoteController()
    {
        note = new NoteRepositoryMock();
        elevi = new EleviRepositoryMock();
    }

    public void addNota( Nota nota ) throws ClasaException
    {
        if( !elevi.getElevi().contains( new Elev( nota.getNrmatricol(), "" ) ) )
        {
            throw new ClasaException( Constants.invalidNrmatricol );
        }
        note.addNota( nota );
    }

    public void addElev( Elev elev ) throws ClasaException
    {
        elevi.addElev( elev );
    }

    public List< Medie > calculeazaMedii() throws ClasaException
    {
        List< Medie > medii = new LinkedList< Medie >();
        List< Elev > elevi = getElevi();
        HashMap< Integer, HashMap< String, Pair< Integer, Integer > > > map = new HashMap< Integer, HashMap< String, Pair< Integer, Integer > > >();

        if( elevi.size() > 0 )
        {
            for( Elev elev : elevi )
            {
                map.put( elev.getNrmatricol(), new HashMap< String, Pair< Integer, Integer > >() );
            }
            for( Nota nota : getNote() )
            {
                HashMap< String, Pair< Integer, Integer > > elev_note = map.get( nota.getNrmatricol() );
                if( elev_note == null )
                {
                    throw new ClasaException( Constants.invalidNrmatricol );
                }
                Pair< Integer, Integer > pair = elev_note.get( nota.getMaterie() );
                if( pair == null )
                {
                    elev_note.put( nota.getMaterie(), new Pair< Integer, Integer >( 1, nota.getNota() ) );
                }
                else
                {
                    elev_note.put( nota.getMaterie(), new Pair< Integer, Integer >( 1 + pair.getKey(), nota.getNota() + pair.getValue() ) );
                }
            }
            for( Elev elev : elevi )
            {
                HashMap< String, Pair< Integer, Integer > > elev_note = map.get( elev.getNrmatricol() );
                int nr_materii = 0;
                int nr_corigente = 0;
                double suma_medii = 0;
                double media;
                for( Pair< Integer, Integer > pair : elev_note.values() )
                {
                    nr_materii++;
                    media = ( double )pair.getValue() / pair.getKey();
                    if( media < Constants.minNotaTrecere )
                    {
                        nr_corigente++;
                    }
                    suma_medii += ( int )( media + 0.5 );
                }
                if( nr_materii == 0 )
                {
                    medii.add( new Medie( elev, -1, nr_corigente ) );
                }
                else
                {
                    medii.add( new Medie( elev, suma_medii / nr_materii, nr_corigente ) );
                }
            }
        }
        else
        {
            throw new ClasaException( Constants.emptyRepository );
        }
        return medii;
    }

    public List< Nota > getNote()
    {
        return note.getNote();
    }

    public List< Elev > getElevi()
    {
        return elevi.getElevi();
    }

    public void readElevi( String fisier ) throws ClasaException
    {
        FileReaders.readElevi( fisier,this );
    }

    public void readNote( String fisier ) throws ClasaException
    {
        FileReaders.readNote( fisier,this );
    }

    public List< Medie > getCorigenti() throws ClasaException
    {
        List< Medie > corigenti = new ArrayList< Medie >();
        List< Medie > medii = calculeazaMedii();
        for( Medie m : medii )
        {
            if( m.getNr_corigente() > 0 )
            {
                corigenti.add( m );
            }
        }
        corigenti.sort( ( m1, m2 ) ->
                        {
                            return m1.getElev().getNume().compareTo( m2.getElev().getNume() );
                        } );
        corigenti.sort( ( m1, m2 ) ->
                        {
                            return m2.getNr_corigente() - m1.getNr_corigente();
                        } );
        return corigenti;
    }
}
