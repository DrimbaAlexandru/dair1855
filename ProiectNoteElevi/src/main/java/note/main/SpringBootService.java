package note.main;

/**
 * Created by Alex on 13.05.2018.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan("note.RestControllers")
public class SpringBootService
{
    public static void main( String[] args )
    {
        SpringApplication.run( SpringBootService.class, args );
    }
}