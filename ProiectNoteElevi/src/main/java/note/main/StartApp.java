package note.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import note.model.Nota;
import note.utils.ClasaException;

import note.model.Medie;

import note.controller.NoteController;

//functionalitati
//i.	 adaugarea unei note la o anumita materie (nr. matricol, materie, nota acordata);
//ii.	 calcularea mediilor semestriale pentru fiecare elev (nume, nr. matricol),
//iii.	 afisarea elevilor coringenti, ordonati descrescator dupa numarul de materii la care nu au promovat şi alfabetic dupa nume.


public class StartApp
{
    /**
     * @param args
     * @throws ClasaException
     */
    public static void main2( String[] args )
    {
        // TODO Auto-generated method stub
        NoteController ctrl = new NoteController();
        boolean should_exit = false;
        Scanner scanner = new Scanner( System.in );
        try
        {
            if( args.length < 2 )
            {
                throw new ClasaException( "Numar insuficient de argumente" );
            }
            ctrl.readElevi( args[ 0 ] );
            ctrl.readNote( args[ 1 ] );

            while( !should_exit )
            {
                System.out.println( "1. Adaugare Nota" );
                System.out.println( "2. Calculeaza medii" );
                System.out.println( "3. Elevi corigenti" );
                System.out.println( "4. Iesire" );

                try
                {
                    int option = scanner.nextInt();
                    switch( option )
                    {
                        case 1:
                            int nota;
                            String materie;
                            int nr_matricol;

                            System.out.println( "Nr. matricol: " );
                            nr_matricol = scanner.nextInt();
                            System.out.println( "Nume materie: " );
                            materie = scanner.next();
                            System.out.println( "Nota: " );
                            nota = scanner.nextInt();
                            Nota new_nota = new Nota( nr_matricol, materie, nota );
                            ctrl.addNota( new_nota );
                            break;
                        case 2:
                            List< Medie > medii = ctrl.calculeazaMedii();
                            for( Medie medie : medii )
                            {
                                System.out.println( medie );
                            }
                            break;
                        case 3:
                            List< Medie > corigenti = ctrl.getCorigenti();
                            for( Medie m : corigenti )
                            {
                                System.out.println( m.getElev().getNume() + " -> " + m.getNr_corigente() + " corigente" );
                            }
                            break;
                        case 4:
                            should_exit = true;
                            break;
                        default:
                            System.out.println( "Introduceti o optiune valida!" );
                    }
                }
                catch( InputMismatchException e )
                {
                    System.err.println( "Eroare de formatare. Datele introduse nu sunt valide." );
                    //e.printStackTrace();
                }
                catch( ClasaException e )
                {
                    System.err.println( e.getMessage() );
                }
                catch( Exception e )
                {
                    e.printStackTrace();
                }
            }
        }
        catch( ClasaException e )
        {
            System.err.println( e.getMessage() );
        }
    }
}
