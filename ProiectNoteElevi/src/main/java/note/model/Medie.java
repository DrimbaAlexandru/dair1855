package note.model;

import note.utils.Constants;

public class Medie
{
	private Elev elev;
	private double medie;
    private int nr_corigente;

    public Medie( Elev elev, double medie, int nr_corigente )
    {
        this.elev = elev;
        this.medie = medie;
        this.nr_corigente = nr_corigente;
    }

	/**
	 * @return the elev
	 */
	public Elev getElev()
	{
		return elev;
	}

	/**
	 * @param elev the elev to set
	 */
	public void setElev( Elev elev )
	{
		this.elev = elev;
	}

	/**
	 * @return the medie
	 */
	public double getMedie()
	{
		return medie;
	}

	/**
	 * @param medie the medie to set
	 */
	public void setMedie( double medie )
	{
		this.medie = medie;
	}

	public String toString()
	{
        if( medie > 0 )
        {
            return this.elev.getNume() + " -> " + this.medie;
        }
        else
        {
            return this.elev.getNume() + " -> "+ Constants.noGradesForMedie;
        }
	}

    public int getNr_corigente()
    {
        return nr_corigente;
    }

    public void setNr_corigente( int nr_corigente )
    {
        this.nr_corigente = nr_corigente;
    }

	@Override
	public boolean equals( Object obj )
	{
        return ( obj instanceof Medie ) && ( ( ( Medie )obj ).elev == elev ) && ( ( ( Medie )obj ).nr_corigente == nr_corigente ) && ( ( ( Medie )obj ).medie == medie );
    }
}
