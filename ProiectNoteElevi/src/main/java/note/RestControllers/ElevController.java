package note.RestControllers;

/**
 * Created by Alex on 13.05.2018.
 */
import com.fasterxml.jackson.annotation.JsonSubTypes;
import note.controller.NoteController;
import note.model.Elev;
import note.model.Nota;
import note.utils.ClasaException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ElevController
{
    private static final NoteController ctr = new NoteController();
    private static boolean initialized = false;

    public ElevController()
    {
        if( initialized )
        {
            return;
        }

        try
        {
            initialized = true;
            ctr.addElev( new Elev( 10, "Elisei1" ) );
            ctr.addElev( new Elev( 11, "Elisei2" ) );
            ctr.addElev( new Elev( 12, "Elisei3" ) );
            ctr.addElev( new Elev( 13, "Elisei4" ) );

            ctr.addNota( new Nota( 10, "Materie1", 4 ) );
            ctr.addNota( new Nota( 10, "Materie1", 5 ) );
            ctr.addNota( new Nota( 10, "Materie1", 4 ) );
        }
        catch( ClasaException e )
        {
            e.printStackTrace();
        }
    }

    @RequestMapping( value = "/elev", method = RequestMethod.GET )
    @CrossOrigin
    public Iterable< Elev > getAllElev()
    {
        return ctr.getElevi();
    }

    @RequestMapping( value = "/nota", method = RequestMethod.GET )
    @CrossOrigin
    public Iterable< Nota > getAllNote()
    {
        return ctr.getNote();
    }

    @RequestMapping( value = "/nota", method = RequestMethod.POST )
    @CrossOrigin
    public ResponseEntity< ErrorResponse > addNota( @RequestBody Nota nota )
    {
        try
        {
            ctr.addNota( nota );
        }
        catch( ClasaException e )
        {
            return new ResponseEntity< ErrorResponse >( new ErrorResponse( e.getMessage() ), HttpStatus.BAD_REQUEST );
        }
        return new ResponseEntity< ErrorResponse >( HttpStatus.OK );
    }
}
