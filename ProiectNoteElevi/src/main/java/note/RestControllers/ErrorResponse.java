package note.RestControllers;

/**
 * Created by Alex on 13.05.2018.
 */
public class ErrorResponse
{
    private String error = "";

    public void setError( String error )
    {
        this.error = error;
    }

    public String getError()
    {
        return error;
    }

    public ErrorResponse()
    {
        error = "";
    }

    public ErrorResponse( String msg )
    {
        error = msg;
    }
}