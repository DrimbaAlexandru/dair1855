package note.utils;

public class Constants {
	public static int minNota = 1;
	public static int maxNota = 10;
	public static int minNrmatricol = 1;
	public static int maxNrmatricol = 1000;
    public static int minMaterieLen = 5;
    public static int maxMaterieLen = 20;
    public static int maxElevNumeLen = 64;
    public static double minNotaTrecere = 4.5;
    public static String invalidNota = "Nota introdusa nu este corecta";
	public static String invalidMateria = "Lungimea materiei este invalida";
	public static String invalidNrmatricol = "Numarul matricol introdus nu este corect";
	public static String emptyRepository = "Nu exista date";
	public static String noGradesForMedie = "Nu se poate calcula media; nu exista note la nicio materie";
    public static String invalidElevNume = "Nume invalid";
}
