package note.repository;

import note.controller.NoteController;
import note.model.Elev;
import note.model.Nota;
import note.utils.ClasaException;

import java.io.*;
import java.util.List;

/**
 * Created by Alex on 08.03.2018.
 */
public class FileReaders
{
    public static void readElevi( String fisier, NoteController ctrlr ) throws ClasaException
    {
        try
        {
            FileInputStream fstream = new FileInputStream( fisier );
            DataInputStream in = new DataInputStream( fstream );
            BufferedReader br = new BufferedReader( new InputStreamReader( in ) );
            String line;
            while( ( line = br.readLine() ) != null )
            {
                Elev elev;
                try
                {
                    String[] values = line.split( ";" );
                    elev = new Elev( Integer.parseInt( values[ 0 ] ), values[ 1 ] );
                }
                catch( Exception e )
                {
                    throw new ClasaException( "Erorare la parsarea din fisier. " + e.getMessage() );
                }
                ctrlr.addElev( elev );
            }
            br.close();
        }
        catch( FileNotFoundException e )
        {
            throw new ClasaException( "Erorare la parsarea din fisier. " + e.getMessage() );
        }
        catch( IOException e )
        {
            throw new ClasaException( "Erorare la parsarea din fisier. " + e.getMessage() );
        }
    }

    public static void readNote( String fisier, NoteController ctrlr ) throws ClasaException
    {
        try
        {
            FileInputStream fstream = new FileInputStream( fisier );
            DataInputStream in = new DataInputStream( fstream );
            BufferedReader br = new BufferedReader( new InputStreamReader( in ) );
            String line;
            while( ( line = br.readLine() ) != null )
            {
                Nota nota;
                try
                {
                    String[] values = line.split( ";" );
                    nota = new Nota( Integer.parseInt( values[ 0 ] ), values[ 1 ], Integer.parseInt( values[ 2 ] ) );
                }
                catch( Exception e )
                {
                    throw new ClasaException( "Erorare la parsarea din fisier. " + e.getMessage() );
                }
                ctrlr.addNota( nota );
            }
            br.close();
        }
        catch( FileNotFoundException e )
        {
            throw new ClasaException( "Erorare la parsarea din fisier. " + e.getMessage() );
        }
        catch( IOException e )
        {
            throw new ClasaException( "Erorare la parsarea din fisier. " + e.getMessage() );
        }
    }
}
