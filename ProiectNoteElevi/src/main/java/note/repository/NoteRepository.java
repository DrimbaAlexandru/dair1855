package note.repository;

import note.model.Nota;
import note.utils.ClasaException;

import java.util.List;

public interface NoteRepository
{
	public void addNota( Nota nota ) throws ClasaException;

	public List< Nota > getNote();
}
