package note.repository;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import note.model.Elev;
import note.utils.ClasaException;
import note.utils.Constants;

public class EleviRepositoryMock implements EleviRepository
{
	private List< Elev > elevi;

	public EleviRepositoryMock()
	{
		elevi = new LinkedList< Elev >();
	}

	@Override
	public void addElev( Elev e ) throws ClasaException
	{
        validareElev( e );
        elevi.add( e );
	}

	@Override
	public List< Elev > getElevi()
	{
		return elevi;
	}

    private void validareElev( Elev elev ) throws ClasaException
    {
        if( elev.getNrmatricol() < Constants.minNrmatricol || elev.getNrmatricol() > Constants.maxNrmatricol )
        {
            throw new ClasaException( Constants.invalidNrmatricol );
        }
        if( elev.getNume().length() > Constants.maxElevNumeLen )
        {
            throw new ClasaException( Constants.invalidElevNume );
        }
    }

}
