package note.repository;

import java.util.List;

import note.model.Elev;
import note.utils.ClasaException;

public interface EleviRepository
{
	public void addElev( Elev e ) throws ClasaException;

	public List< Elev > getElevi();
}
