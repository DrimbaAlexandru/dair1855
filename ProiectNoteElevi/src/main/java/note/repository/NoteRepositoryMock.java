package note.repository;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import note.utils.ClasaException;
import note.utils.Constants;

import note.model.Nota;

public class NoteRepositoryMock implements NoteRepository
{
	private List< Nota > note;

	public NoteRepositoryMock()
	{
		note = new LinkedList< Nota >();
	}

	@Override
	public void addNota( Nota nota ) throws ClasaException
	{
		if( !validareNota( nota ) )
		{
			return;
		}
		note.add( nota );
	}

	private boolean validareNota( Nota nota ) throws ClasaException
	{
		if( nota.getMaterie().length() < Constants.minMaterieLen || nota.getMaterie().length() > Constants.maxMaterieLen )
		{
			throw new ClasaException( Constants.invalidMateria );
		}
		if( nota.getNrmatricol() < Constants.minNrmatricol || nota.getNrmatricol() > Constants.maxNrmatricol )
		{
			throw new ClasaException( Constants.invalidNrmatricol );
		}
		if( nota.getNota() < Constants.minNota || nota.getNota() > Constants.maxNota )
		{
			throw new ClasaException( Constants.invalidNota );
		}
		return true;
	}

	@Override
	public List< Nota > getNote()
	{
		return note;
	}

}
