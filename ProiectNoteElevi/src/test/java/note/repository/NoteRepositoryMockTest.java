package note.repository;

import note.model.Nota;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Assert;

import static org.junit.Assert.*;

/**
 * Created by Alex on 24.03.2018.
 */
public class NoteRepositoryMockTest
{
    @org.junit.Test
    public void addNota_TC_01() throws Exception
    {
        final NoteRepositoryMock repo = new NoteRepositoryMock();
        final int nrMatricol = 250;
        final String materie = "Desen";
        final int nota = 10;
        ClasaException exception = null;

        Assert.assertEquals( repo.getNote().size(), 0 );
        try
        {
            repo.addNota( new Nota( nrMatricol,materie,nota ) );
        }
        catch( ClasaException ex )
        {
            exception = ex;
        }
        Assert.assertEquals( exception, null );
        Assert.assertEquals( repo.getNote().size(), 1 );
    }

    @org.junit.Test
    public void addNota_TC_03() throws Exception
    {
        final NoteRepositoryMock repo = new NoteRepositoryMock();
        final int nrMatricol = 250;
        final String materie = "Info";
        final int nota = 10;
        ClasaException exception = null;

        Assert.assertEquals( repo.getNote().size(), 0 );
        try
        {
            repo.addNota( new Nota( nrMatricol,materie,nota ) );
        }
        catch( ClasaException ex )
        {
            exception = ex;
        }
        Assert.assertNotEquals( exception, null );
        if( exception != null )
        {
            Assert.assertEquals( exception.getMessage(), Constants.invalidMateria );
        }
        Assert.assertEquals( repo.getNote().size(), 0 );
    }

    @org.junit.Test
    public void addNota_TC_13() throws Exception
    {
        final NoteRepositoryMock repo = new NoteRepositoryMock();
        final int nrMatricol = 250;
        final String materie = "Materie";
        final int nota = 1;
        ClasaException exception = null;

        Assert.assertEquals( repo.getNote().size(), 0 );
        try
        {
            repo.addNota( new Nota( nrMatricol,materie,nota ) );
        }
        catch( ClasaException ex )
        {
            exception = ex;
        }
        Assert.assertEquals( exception, null );
        Assert.assertEquals( repo.getNote().size(), 1 );
    }

    @org.junit.Test
    public void addNota_TC_18() throws Exception
    {
        final NoteRepositoryMock repo = new NoteRepositoryMock();
        final int nrMatricol = 250;
        final String materie = "Materie";
        final int nota = 11;
        ClasaException exception = null;

        Assert.assertEquals( repo.getNote().size(), 0 );
        try
        {
            repo.addNota( new Nota( nrMatricol,materie,nota ) );
        }
        catch( ClasaException ex )
        {
            exception = ex;
        }
        Assert.assertNotEquals( exception, null );
        if( exception != null )
        {
            Assert.assertEquals( exception.getMessage(), Constants.invalidNota );
        }
        Assert.assertEquals( repo.getNote().size(), 0 );
    }
}