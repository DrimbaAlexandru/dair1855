package note.test;

import note.controller.NoteController;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.repository.NoteRepositoryMockTest;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Alex on 22.04.2018.
 */
public class IntegrationTest_TopDown
{
    NoteController ctrl;

    @Before
    public void setUp() throws Exception
    {
        ctrl = new NoteController();
    }

    //Unit test: Module A (cerinta I)
    @org.junit.Test
    public void addNota_UnitTest() throws Exception
    {
        Result result = JUnitCore.runClasses( NoteRepositoryMockTest.class );
        assertTrue( result.wasSuccessful() );
    }

    //Integration test: P->A->B
    @Test
    public void moduleB_int_test() throws Exception
    {
        Elev e1 = new Elev( 1, "Elev1" );
        Elev e2 = new Elev( 2, "Elev2" );
        try
        {
            ctrl.addElev( e1 );
            ctrl.addElev( e2 );

            assertEquals( ctrl.getElevi().size(), 2 );

            ctrl.addNota( new Nota( 1, "Materie1", 10 ) );
            ctrl.addNota( new Nota( 1, "Materie1", 9 ) );
            ctrl.addNota( new Nota( 2, "Materie1", 8 ) );
            ctrl.addNota( new Nota( 2, "Materie1", 11 ) );
            assertFalse( true );
        }
        catch( ClasaException e )
        {
            assertEquals( e.getMessage(), Constants.invalidNota );
        }
        assertEquals( ctrl.getNote().size(), 3 );

        Medie expM1 = new Medie( e1, 10, 0 );
        Medie expM2 = new Medie( e2, 8, 0 );
        List< Medie > medii = ctrl.calculeazaMedii();
        assertEquals( medii.size(), 2 );
        assertEquals( medii.contains( expM1 ), true );
        assertEquals( medii.contains( expM2 ), true );

    }

    //Integration test: P->A->B->C
    @Test
    public void moduleBC_int_test() throws Exception
    {
        Elev e1 = new Elev( 1, "Elev1" );
        Elev e2 = new Elev( 2, "Elev2" );

        ctrl.addElev( e1 );
        ctrl.addElev( e2 );

        assertEquals( ctrl.getElevi().size(), 2 );

        ctrl.addNota( new Nota( 1, "Materie1", 10 ) );
        ctrl.addNota( new Nota( 1, "Materie1", 9 ) );
        ctrl.addNota( new Nota( 1, "Materie1", 8 ) );
        ctrl.addNota( new Nota( 2, "Materie1", 6 ) );
        ctrl.addNota( new Nota( 2, "Materie1", 5 ) );
        ctrl.addNota( new Nota( 2, "Materie1", 2 ) );

        assertEquals( ctrl.getNote().size(), 6 );

        Medie expM1 = new Medie( e1, 9, 0 );
        Medie expM2 = new Medie( e2, 4, 1 );
        List< Medie > medii = ctrl.calculeazaMedii();
        assertEquals( medii.size(), 2 );
        assertEquals( medii.contains( expM1 ), true );
        assertEquals( medii.contains( expM2 ), true );

        List< Medie > corigente = ctrl.getCorigenti();
        assertEquals( corigente.size(), 1 );
        assertEquals( corigente.get( 0 ), expM2 );
    }
}
