package note.test;

import static org.junit.Assert.*;

import java.util.List;

import note.controller.NoteControllerTest;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;

import note.repository.NoteRepositoryMock;
import note.repository.NoteRepositoryMockTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import note.utils.ClasaException;
import note.utils.Constants;

import note.controller.NoteController;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

public class IntegrationTest_BigBang
{
	NoteController ctrl;

	@Before
	public void setUp() throws Exception
	{
		ctrl = new NoteController();
	}

	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

    //Unit test: Module A (cerinta I)
    @org.junit.Test
    public void addNota_UnitTest() throws Exception
    {
        Result result = JUnitCore.runClasses( NoteRepositoryMockTest.class );
        assertTrue( result.wasSuccessful() );
    }

    //Unit test: Module B+C (cerinta II+III)
    @Test
    public void module_B_C_UnitTest() throws Exception
    {
        Result result = JUnitCore.runClasses( NoteControllerTest.class );
        assertTrue( result.wasSuccessful() );
    }

    //Integration test: P->A->B->C
    @Test
    public void IntegrationTest() throws Exception
    {
        NoteController ctr = new NoteController();

        //Integration Test: Module A
        Elev e1 = new Elev( 1, "Elev1" );
        Elev e2 = new Elev( 2, "Elev2" );

        ctr.addElev( e1 );
        ctr.addElev( e2 );

        ctr.addNota( new Nota( 1, "Materia1", 10 ) );
        ctr.addNota( new Nota( 1, "Materia1", 9 ) );
        ctr.addNota( new Nota( 1, "Materia2", 10 ) );
        ctr.addNota( new Nota( 1, "Materia2", 7 ) );

        ctr.addNota( new Nota( 2, "Materia1", 7 ) );
        ctr.addNota( new Nota( 2, "Materia1", 9 ) );
        ctr.addNota( new Nota( 2, "Materia1", 1 ) );

        assertEquals( ctr.getElevi().size(), 2 );
        assertEquals( ctr.getNote().size(), 7 );

        //Integration Test: Module B
        Medie expMedie1 = new Medie( e1, 9.50, 0 );
        Medie expMedie2 = new Medie( e2, 6.00, 0 );

        List< Medie > medii = ctr.calculeazaMedii();
        assertEquals( medii.size(), 2 );
        assertEquals( medii.contains( expMedie1 ), true );
        assertEquals( medii.contains( expMedie2 ), true );

        //Integration Test: Module C
        ctr.addNota( new Nota( 2, "Materia1", 3 ) );
        ctr.addNota( new Nota( 2, "Materia1", 2 ) );

        List< Medie > corigente = ctr.getCorigenti();
        Medie expCorigenta = new Medie( e2, 4, 1 );

        assertEquals( corigente.size(), 1 );
        assertEquals( corigente.get( 0 ), expCorigenta );
    }

}
