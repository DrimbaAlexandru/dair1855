package note.controller;

import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Alex on 15.04.2018.
 */
public class NoteControllerTest
{
    @Test
    public void calculeazaMedii_TC_01() throws Exception
    {
        try
        {
            NoteController ctr = new NoteController();
            Elev e = new Elev( 3, "Elev3" );
            Nota n1 = new Nota( 3, "Materia2", 4 );
            Nota n2 = new Nota( 3, "Materia2", 4 );
            Medie expectedMedie = new Medie( e, 4, 1 );
            Medie actualMedie;
            ctr.getElevi().add( e );
            ctr.getNote().add( n1 );
            ctr.getNote().add( n2 );

            assertEquals( ctr.getElevi().size(), 1 );
            assertEquals( ctr.getNote().size(), 2 );

            List< Medie > medii = ctr.calculeazaMedii();
            assertEquals( medii.size(), 1 );
            actualMedie = medii.get( 0 );
            assertTrue( actualMedie.getElev() == expectedMedie.getElev() );
            assertEquals( actualMedie.getMedie(), expectedMedie.getMedie(), 0 );
            assertEquals( actualMedie.getNr_corigente(), expectedMedie.getNr_corigente() );
        }
        catch( ClasaException e )
        {
            assertTrue( false );
        }
    }

    @Test
    public void calculeazaMedii_TC_02() throws Exception
    {
        try
        {
            NoteController ctr = new NoteController();

            assertEquals( ctr.getElevi().size(), 0 );
            assertEquals( ctr.getNote().size(), 0 );

            List< Medie > medii = ctr.calculeazaMedii();
            assertFalse( true );
        }
        catch( ClasaException e )
        {
            assertEquals( e.getMessage(), Constants.emptyRepository );
        }
    }

    @Test
    public void calculeazaMedii_TC_03() throws Exception
    {
        try
        {
            NoteController ctr = new NoteController();
            Elev e = new Elev( 3, "Elev3" );
            Nota n1 = new Nota( 3, "Materia2", 5 );
            Nota n2 = new Nota( 3, "Materia2", 4 );
            Medie expectedMedie = new Medie( e, 5, 0 );
            Medie actualMedie;
            ctr.getElevi().add( e );
            ctr.getNote().add( n1 );
            ctr.getNote().add( n2 );

            assertEquals( ctr.getElevi().size(), 1 );
            assertEquals( ctr.getNote().size(), 2 );

            List< Medie > medii = ctr.calculeazaMedii();
            assertEquals( medii.size(), 1 );
            actualMedie = medii.get( 0 );
            assertTrue( actualMedie.getElev() == expectedMedie.getElev() );
            assertEquals( actualMedie.getMedie(), expectedMedie.getMedie(), 0 );
            assertEquals( actualMedie.getNr_corigente(), expectedMedie.getNr_corigente() );
        }
        catch( ClasaException e )
        {
            assertTrue( false );
        }
    }

    @Test
    public void calculeazaMedii_TC_04() throws Exception
    {
        try
        {
            NoteController ctr = new NoteController();
            Elev e = new Elev( 3, "Elev3" );
            Medie expectedMedie = new Medie( e, -1, 0 );
            Medie actualMedie;
            ctr.getElevi().add( e );

            assertEquals( ctr.getElevi().size(), 1 );
            assertEquals( ctr.getNote().size(), 0 );

            List< Medie > medii = ctr.calculeazaMedii();
            assertEquals( medii.size(), 1 );
            actualMedie = medii.get( 0 );
            assertTrue( actualMedie.getElev() == expectedMedie.getElev() );
            assertEquals( actualMedie.getMedie(), expectedMedie.getMedie(), 0 );
            assertEquals( actualMedie.getNr_corigente(), expectedMedie.getNr_corigente() );
        }
        catch( ClasaException e )
        {
            assertTrue( false );
        }
    }

    @Test
    public void calculeazaMedii_TC_05() throws Exception
    {
        try
        {
            NoteController ctr = new NoteController();
            Elev e = new Elev( 3, "Elev3" );
            Nota n1 = new Nota( 1, "Materia2", 5 );
            ctr.getElevi().add( e );
            ctr.getNote().add( n1 );

            assertEquals( ctr.getElevi().size(), 1 );
            assertEquals( ctr.getNote().size(), 1 );

            List< Medie > medii = ctr.calculeazaMedii();
            assertTrue( false );
        }
        catch( ClasaException e )
        {
            assertEquals( e.getMessage(),Constants.invalidNrmatricol );
        }
    }

    @Test
    public void getCorigenti_TC_01() throws Exception
    {
        try
        {
            NoteController ctr = new NoteController();
            Elev e = new Elev( 1, "Elev1" );
            Nota n = new Nota( 2, "Materie1", 10 );
            ctr.getElevi().add( e );
            ctr.getNote().add( n );
            List< Medie > corigenti = ctr.getCorigenti();
            assertTrue( false );
        }
        catch( ClasaException e )
        {
            assertEquals( e.getMessage(), Constants.invalidNrmatricol );
        }
    }

    @Test
    public void getCorigenti_TC_02() throws Exception
    {
        try
        {
            NoteController ctr = new NoteController();
            Elev e = new Elev( 1, "Elev1" );
            Nota n1 = new Nota( 1, "Materie1", 4 );
            Nota n2 = new Nota( 1, "Materie1", 5 );
            ctr.getElevi().add( e );
            ctr.getNote().add( n1 );
            ctr.getNote().add( n2 );
            List< Medie > corigenti = ctr.getCorigenti();
            assertTrue( corigenti.size() == 0 );
        }
        catch( ClasaException e )
        {
            assertFalse( true );
        }
    }
}